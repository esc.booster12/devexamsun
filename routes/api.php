<?php

use Illuminate\Support\Facades\Route;

// Route::get('articles', 'ArticleController@all');
Route::post('articles', 'ArticleController@store');
Route::put('articles/{id}', 'ArticleController@update');
Route::delete('articles/{id}', 'ArticleController@destroy');
