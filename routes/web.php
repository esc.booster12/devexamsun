<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('article/downvote/{id}', 'ArticleController@downvote')->name('articles.downvote');
Route::get('article/upvote/{id}', 'ArticleController@upvote')->name('articles.upvote');

Route::get('/', 'ArticleController@index')->name('articles.index');
Route::post('articles', 'ArticleController@store')->name('articles.store');
Route::put('articles/{id}', 'ArticleController@update')->name('articles.update');
Route::get('articles/{id}', 'ArticleController@destroy')->name('articles.delete');


Route::get('articles/create', 'ArticleController@create')->name('articles.create');
Route::get('articles/edit/{id}', 'ArticleController@edit')->name('articles.edit');

