<?php

namespace App\Repositories;

use App\Article;
use Illuminate\Http\Request;

class ArticleRepository extends BaseRepository
{
	public function __construct(Article $model) {
		parent::__construct($model);
	}

	public function createArticle(Request $request) {
		$article = new Article;
    	$article->title = $request->input('title');
    	$article->content = $request->input('content');
    	$article->save();

    	return $article;
	}

	public function updateArticle(Request $request, int $id) {
		$article = $this->findOrFail($id);
    	
    	if($request->input('title')) {
    		$article->title = $request->input('title');
    	}

    	if($request->input('content')) {
    		$article->content = $request->input('content');
    	}
    	
    	$article->save();

    	return $article;
	}

	public function deleteArticle(int $id) {
		$article = $this->findOrFail($id);
		$article->delete();
	}

    public function upvoteArticle(int $id) {
        $article = $this->findOrFail($id);
        $article->vote = $article->vote + 1;
        $article->save();
    }

    public function downvoteArticle(int $id) {
        $article = $this->findOrFail($id);
        $article->vote = $article->vote - 1;
        $article->save();
    }
}