<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
	protected $model;

	public function __construct(Model $model) {
		$this->model = $model;
	}

	public function delete(int $id) {
		return $this->model->find($id)->delete();	
	}

	public function all() {
		return $this->model->all();		
	}	

	public function find(int $id) {
		return $this->model->find($id);
	}

	public function findOrFail(int $id) {
		return $this->model->findOrFail($id);
	}

	public function findBy(array $data) {
		return $this->model->where($data)->all();
	}

	public function findOneBy(array $data) {
		return $this->model->where($data)->first();
	}

	public function findOneByOrFail(array $data) {
		return $this->model->where($data)->firstOrFail();
	}
}