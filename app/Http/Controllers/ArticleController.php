<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepository;

class ArticleController extends Controller
{
	protected $articleRepository;

	public function __construct(ArticleRepository $articleRepository) {
		$this->articleRepository = $articleRepository;
	}

	public function index() {
        $articles = $this->articleRepository->all();

		return view('articles.index', compact('articles'));
	}

    public function create() {
        return view('articles.create');
    }

    public function edit(int $id) {
        $article = $this->articleRepository->findOrFail($id);

        return view('articles.edit', compact('article'));
    }

    public function store() {
    	$this->validate(request(), [
    		'title' => 'required|max:6000',
    		'content' => 'required|max:15000000'
    	]);
    	
    	$this->articleRepository->createArticle(request());

        $articles = $this->articleRepository->all();

        return redirect()->route('articles.index');
    }

    public function update(int $id) {
    	$this->validate(request(), [
    		'title' => 'nullable|max:6000',
    		'content' => 'nullable|max:15000000'
    	]);	

    	$data = $this->articleRepository->updateArticle(request(), $id);

    	return redirect()->route('articles.index');
    }

    public function upvote(int $id) {
        $this->articleRepository->upvoteArticle($id);
    }

    public function downvote(int $id) {
        $this->articleRepository->downvoteArticle($id);
    }

    public function destroy(int $id) {
    	$this->articleRepository->deleteArticle($id);

        return redirect()->route('articles.index');
    }
}
