<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        
        
        <div class="container" id="app">
            @foreach($articles AS $a)
                <div class="card mb-4">
                    <div class="card-header">
                        <h4>{{ $a->title }}</h4>
                    </div>
                    <div class="card-body">
                        {{ $a->content }}
                    </div>
                </div>
            @endforeach
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
