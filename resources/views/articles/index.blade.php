<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        
        
        <div class="container" id="app">
            
            <a href="{{ route('articles.create') }}" class="btn btn-success">Create a Post</a>

            @foreach($articles AS $a)
                <div class="card mt-4">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h4>{{ $a->title }}</h4>
                            </div>
                            <div class="col-6" align="right">
                                <p>{{ $a->created_at->toDayDateTimeString() }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {{ $a->content }}
                    </div>
                    <div class="card-footer">
                        <div class="col-6">
                            # of votes: {{ $a->votes }}
                            <a href="{{ route('articles.downvote', $a->id) }}">downvote</a>
                            <a href="{{ route('articles.upvote', $a->id) }}">upvote</a>
                        </div>
                        <div class="col-6">
                            <a href="{{ route('articles.edit', $a->id) }}" class="btn btn-secondary">Edit</a>
                           <a href="{{ route('articles.delete', $a->id) }}" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
