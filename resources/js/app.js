import Vue from 'vue';
import store from './store';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';

Vue.component('articles', require('./components/Articles.vue').default);

const app = new Vue({
    el: '#app',
    store
});
