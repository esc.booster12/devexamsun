import { http } from './http_service';

const state = {
	articles: {}
};

const getters = {
	articles: state => state.articles
};

const actions = {
	all({commit,state}) {
		return http.get('articles');
	}
}

const mutations = {};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}